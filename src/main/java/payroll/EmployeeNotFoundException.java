package payroll;

import org.omg.SendingContext.RunTime;

public class EmployeeNotFoundException  extends RuntimeException {
    public EmployeeNotFoundException(Long id) {
        super("Could not find employee " + id);
    }
}
